# [![Homepage](https://s3.amazonaws.com/assets.heroku.com/addons.heroku.com/icons/1347/original.png)](https://www.informantapp.com) The Informant

## Supported Platforms

### Ruby

The Informant supports all officially supported versions of
[Ruby](https://www.ruby-lang.org). We provide agents for easy integration with
popular web frameworks.

| Framework | Agent | Example App |
| --------- | ----- | ----------- |
| [Ruby on Rails](https://rubyonrails.org) | [informant-rails](https://gitlab.com/informantapp/informant-rails) | [informant-rails-example](https://gitlab.com/informantapp/informant-rails-example) |

### Additional Platforms

Wish your favorite platform was officially supported? Email us at support@informantapp.com and let us know!

### Collector API

The agent libraries all report data to our collector API.

#### Authentication

All requests must contain the customer's API token in an Authorizataion header in this format.

```
Authorization: Token token="643aa2e615657f7befac0324582121d0"
```

#### Agent Info

When a process starts with the agent enabled it sends a notification to our system with some basic data about the customer system for our support team. This data is never shared with anyone under any circumstances.

```
POST https://collector-api.informantapp.com/v2/client-info

{
  agent_identifier: 'informant-rails-2.0.0',
  framework_version: 'rails-4.2.1',
  runtime_version: 'ruby-2.5.1'
}
```

#### Form Submission

Agents post to this endpoint whenever they detect a valid form submission in a customer application.

This indicates a validation error where the User model was invalid but the Address model was invalid.

```
POST https://collector-api.informantapp.com/v2/form-submissions

{
  name: 'POST /user-sessions',
  models: [
    {
      name: 'User',
      errors: [
        { name: 'email', value: 'bad', message: 'is invalid' },
        { name: 'password_confirmation', value: null, message: "can't be blank" }
      ]
    },
    {
      name: 'Address',
      errors: []
    }
  ]
}
```

This indicates a successful request where the User and Address model were both valid.

```
POST https://collector-api.informantapp.com/v2/form-submissions

{
  name: 'POST /user-sessions',
  models: [
    {
      name: 'User',
      errors: []
    },
    {
      name: 'Address',
      errors: []
    }
  ]
}
```
